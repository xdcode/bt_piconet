package com.example.bluetoothpiconet;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BluetoothStatusReceiver extends BroadcastReceiver {
	private final static String TAG = "btReceiver";
	BluetoothChat callback;
	
	public BluetoothStatusReceiver(BluetoothChat callback) {
		this.callback = callback;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		final String action = intent.getAction();

        if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                                                 BluetoothAdapter.ERROR);
            switch (state) {
            case BluetoothAdapter.STATE_OFF:
                Log.d(TAG,"Bluetooth off");
                
                break;
            case BluetoothAdapter.STATE_TURNING_OFF:
                Log.d(TAG,"Turning Bluetooth off...");
                break;
            case BluetoothAdapter.STATE_ON:
                Log.d(TAG,"Bluetooth on");
                callback.setupChat();
                
                break;
            case BluetoothAdapter.STATE_TURNING_ON:
                Log.d(TAG,"Turning Bluetooth on...");
                break;
            }
        }
    }
}

