
package com.example.bluetoothpiconet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import com.example.bluetoothpiconet.proto.MessageProtos.Msg;
import com.example.bluetoothpiconet.proto.MessageProtos.Msg.MessageType;
import com.google.protobuf.InvalidProtocolBufferException;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class BluetoothChatService {
    // Debugging
    private static final String TAG = "BluetoothChatService";
    private static final boolean D = true;

    // Name for the SDP record when creating server socket
    private static final String NAME = "BluetoothChatMulti";

    // Member fields
    private final BluetoothAdapter mAdapter;
    private final Handler mHandler;
    private AcceptThread mAcceptThread;
    private ConnectThread mConnectThread;
    private int mState;
    private int attemps;
    private HashMap<BluetoothSocket,ConnectedThread> mConnThreads;
    private Set<String> mAddressDevices;
    /**
     * A bluetooth piconet can support up to 7 connections (7 slaves + 1 master).
     *  But only a UUID is needed. When attempting to make a connection, 
     *  the client (master) must match with that the server (slave) is listening for.
     *  When accepting incoming connections server listens for one UUID and can establish 
     *  more than one connection over the same UUID. 
     *  When trying to form an outgoing connection, the client (master) tries UUID, 
     *  retry process could be implement, trying more attemps over the same UUID. 
     */
    private UUID mUuid;
    
    // Constants that indicate the current connection state
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device
    
    public static final int NO_CLEAR_CHAT_HISTORY = 4;
    /**
     * Constructor. Prepares a new BluetoothChat session.
     * @param context  The UI Activity Context
     * @param handler  A Handler to send messages back to the UI Activity
     */
    public BluetoothChatService(Context context, Handler handler) {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mState = STATE_NONE;
        mHandler = handler;
        mConnThreads = new HashMap<BluetoothSocket,ConnectedThread>();
        mAddressDevices = new HashSet<String>();
        mUuid = UUID.fromString("b7746a40-c758-4868-aa19-7ac6b3475dfc");
    }

    /**
     * Set the current state of the chat connection
     * @param state  An integer defining the current connection state
     */
    private synchronized void setState(int state) {
    	setState(state, -1);
    }
    
    private synchronized void setState(int state, int arg2) {
        if (D) Log.d(TAG, "setState() " + mState + " -> " + state);
        mState = state;
        // Give the new state to the Handler so the UI Activity can update
        mHandler.obtainMessage(BluetoothChat.MESSAGE_STATE_CHANGE, state, arg2).sendToTarget();
    }
    
    private synchronized void setState(int state, String reason) {
    	String strStateNew ="";
    	String strStateold ="";
    	
    	switch (state){
    	case STATE_NONE:
    		strStateNew = "idle";
    		break;
    	case STATE_LISTEN:
    		strStateNew = "listening";
    		break;
    	case STATE_CONNECTING:
    		strStateNew = "connecting";
    		break;
    	case STATE_CONNECTED:
    		strStateNew = "connected";
    		break;
    	}
    	
    	switch (mState){
    	case STATE_NONE:
    		strStateold = "idle";
    		break;
    	case STATE_LISTEN:
    		strStateold = "listening";
    		break;
    	case STATE_CONNECTING:
    		strStateold = "connecting";
    		break;
    	case STATE_CONNECTED:
    		strStateold = "connected";
    		break;
    	}
    	
    	if (D) Log.d(TAG, "setState() " + strStateold + " -> " + strStateNew + ", reason: " + reason);
    	setState(state);
    }

    /**
     * Return the current connection state. */
    public synchronized int getState() {
        return mState;
    }

    /**
     * Start the chat service. Specifically start AcceptThread to begin a
     * session in listening (server / slave) mode . Called by the Activity onResume() */
    public synchronized void start() {
        if (D) Log.d(TAG, "start");

        // Cancel any thread attempting to make a connection
        if (mConnectThread != null) {
        	Log.d(TAG, "connect thread " + mConnectThread.getName() + " is about to cancel");
        	mConnectThread.cancel();
        	mConnectThread = null;
        }
        // Cancel any thread currently running a connection. Iterate connectedThread set cancel and remove it
        if (mConnThreads.size() > 0) {
        	Iterator<Entry<BluetoothSocket, ConnectedThread>> it = mConnThreads.entrySet().iterator();
            while (it.hasNext()) {
            	Entry<BluetoothSocket, ConnectedThread> entrie = it.next();
            	ConnectedThread connectedThread = entrie.getValue();
            	connectedThread.cancel();
            	connectedThread = null;
            }
            mConnThreads.clear();
            mAddressDevices.clear();
        }

        // Start the thread to listen on a BluetoothServerSocket
        if (mAcceptThread == null) {
            mAcceptThread = new AcceptThread();
            mAcceptThread.start();
        }
        setState(STATE_LISTEN, "start");
    }
    
    public synchronized void start(UUID a) {
    	if (D) Log.d(TAG, "start uuid: "+ a.toString());
    	start();
    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     * @param device  The BluetoothDevice to connect
     */
    public synchronized void connect(BluetoothDevice device) {
        if (D) Log.d(TAG, "connect to: " + device);
        
        // Cancel any thread attempting to make a connection
        if (mState == STATE_CONNECTING) {
            if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}
        }
        
        // Create a new thread and attempt to connect with the UUID.    
        	try {
        		mConnectThread = new ConnectThread(device, mUuid);
        		mConnectThread.setName("connect to: " + device.getName() + " attemps:" + (attemps + 1));
        		mConnectThread.start();
        		setState(STATE_CONNECTING,"trying connect to: " + device.getName());
        		//Cannot join to this thread because it calls a method in this thread, result: infinite wait
        	} catch (Exception e) { }
    }

    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     * @param socket  The BluetoothSocket on which the connection was made
     * @param device  The BluetoothDevice that has been connected
     */
    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {
        if (D) Log.d(TAG, "connected");
        
        //TODO Its place should be before trying anything rather. Here we know the other device is accepting the connection
        /*
         * Add each address to a set. 
		 * it's useful for avoid one device is connected twice
         *
        Boolean added = mAddressDevices.add(device.getAddress());
        if (!added) {
        	Message msg = mHandler.obtainMessage(BluetoothChat.MESSAGE_TOAST);
        	Bundle bundle = new Bundle();
        	bundle.putString(BluetoothChat.TOAST, " You're already connecting to this device");
        	msg.setData(bundle);
        	mHandler.sendMessage(msg);
        	
        	setState(STATE_CONNECTED,"connected");
        	return;
        }
        */
        
        // Start the thread to manage the connection and perform transmissions
        ConnectedThread mConnectedThread = new ConnectedThread(socket, device);
        mConnectedThread.start();
        /* Add each connected thread to a hashmap
         * KEY: the socket used in communication
         * VALUE: the connected thread
         */
        mConnThreads.put(socket,mConnectedThread);
        
        for (int i=0; i < mConnThreads.size(); i++) {
        	//String key = (String)mConnThreads.keySet().toArray()[i];
        	ConnectedThread val = (ConnectedThread)mConnThreads.values().toArray()[i];
        	Log.d(TAG, "Connected to: " + val.mmDevice.getName()+ ", using socket: " + val.mmSocket.toString());

        }

        // Send the name of the connected device back to the UI Activity
        Message msg = mHandler.obtainMessage(BluetoothChat.MESSAGE_DEVICE_NAME);
        Bundle bundle = new Bundle();
        bundle.putString(BluetoothChat.DEVICE_NAME, device.getName());
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        setState(STATE_CONNECTED,"connected");
    }


    /**
     * Stop all threads
     */
    public synchronized void stop() {
        if (D) Log.d(TAG, "stop");
        if (mConnectThread != null) {mConnectThread.cancel(); mConnectThread = null;}
        if (mConnThreads.size() > 0) {
        	Iterator<Entry<BluetoothSocket, ConnectedThread>> it = mConnThreads.entrySet().iterator();
            while (it.hasNext()) {
            	Entry<BluetoothSocket, ConnectedThread> entrie = it.next();
            	ConnectedThread connectedThread = entrie.getValue();
            	Log.d(TAG, "about to cancel: " + connectedThread.mmDevice.getName());
            	connectedThread.cancel();
            	connectedThread = null;
            }
            mConnThreads.clear();
            mAddressDevices.clear();
        }
        if (mAcceptThread != null) {mAcceptThread.cancel(); mAcceptThread = null;}
        setState(STATE_NONE, "stop");
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     * @param out The bytes to write
     * @see ConnectedThread#write(byte[])
     */
    public void write(byte[] out) {
    	// When writing, try to write out to all connected threads 
    	Iterator<Entry<BluetoothSocket, ConnectedThread>> it = mConnThreads.entrySet().iterator();
        while (it.hasNext()) {
        	try {
        		Entry<BluetoothSocket, ConnectedThread> entrie = it.next();
        		// Create temporary object
        		ConnectedThread r;
        		// Synchronize a copy of the ConnectedThread
        		synchronized (this) {
        			if (mState != STATE_CONNECTED) return;
        			r = entrie.getValue();
        		}
        		// Perform the write unsynchronized
        		r.write(out);
        	}
        	catch (Exception e) {    			
        	}
        }
    }

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private void connectionFailed() {
    	if (mConnThreads.size() == 0) 
    		setState(STATE_LISTEN, "failed");
    	else 
    		setState(STATE_CONNECTED, "failed");

    	// Send a failure message back to the Activity
    	Message msg = mHandler.obtainMessage(BluetoothChat.MESSAGE_TOAST);
    	Bundle bundle = new Bundle();
    	bundle.putString(BluetoothChat.TOAST, "Unable to connect device");
    	msg.setData(bundle);
    	mHandler.sendMessage(msg);

    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private synchronized void connectionLost(BluetoothSocket socket) {
    	//connected thread
    	Log.d(TAG, "mConnThreads.size:" + mConnThreads.size() + ", mAddressDevices.size:" + mAddressDevices.size());
    	ConnectedThread threadLost = mConnThreads.get(socket);
    	if (threadLost != null) {
    		threadLost.cancel();
    		mConnThreads.remove(socket);
    		mAddressDevices.remove(socket.getRemoteDevice().getAddress());
    	}
    	
    	Log.d(TAG, "mConnThreads.size:" + mConnThreads.size() + ", mAddressDevices.size:" + mAddressDevices.size());
    	// Send a failure message back to the Activity
    	Message msg = mHandler.obtainMessage(BluetoothChat.MESSAGE_TOAST);
    	Bundle bundle = new Bundle();
    	bundle.putString(BluetoothChat.TOAST, "Device connection with " + socket.getRemoteDevice().getName()  + " was lost");
    	msg.setData(bundle);
    	mHandler.sendMessage(msg);

    	msg = mHandler.obtainMessage(BluetoothChat.MESSAGE_CONNECTED);
    	Bundle b = new Bundle();
    	b.putString(BluetoothChat.NUM_CONNECTED, " " + mConnThreads.size());
    	msg.setData(b);
    	mHandler.sendMessage(msg);

    	if (mConnThreads.size() == 0)
    		setState(STATE_LISTEN, "lost");
    	else
    		setState(STATE_CONNECTED, "lost");

    }

    public String info(){
    	/*
    	StringBuffer info = new StringBuffer();
    	info.append(mConnThreads.size());
    	info.append(":");
    	for (ConnectedThread a : mConnThreads) {
    		info.append(a.mmDevice.getName());
    		info.append("/");
    	}
    	return info.toString();
    	*/
    	return "not implemented yet";
    }

    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted
     * (or until cancelled).
     */
    private class AcceptThread extends Thread {
    	BluetoothServerSocket serverSocket = null;
        
        public AcceptThread() {
        }

        public void run() {
            if (D) Log.i(TAG, "BEGIN mAcceptThread " + this);
            setName("AcceptThread");
            BluetoothSocket socket = null;
            try {

            	// Listen for 7 connections over the same UUID
            	//TODO depending if it's a node or a coordinator wait only 1 time or more
            	for (int i = 0; i < 7; i++) {
            		serverSocket = mAdapter.listenUsingRfcommWithServiceRecord(NAME, mUuid);
            		Log.d(TAG,"About to wait, accepting for a client");
                    socket = serverSocket.accept();
                    if (socket != null) {
                    	//String address = socket.getRemoteDevice().getAddress();
	                    connected(socket, socket.getRemoteDevice());
                    }
                    serverSocket.close();
            	}
            } catch (IOException e) {
                Log.e(TAG, "accept() has been interrupted, cause: " + e.getLocalizedMessage());
            }
            if (D) Log.i(TAG, "END mAcceptThread");
        }

        public void cancel() {
            if (D) Log.d(TAG, "cancel " + this);
            try {
                serverSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of server failed", e);
            }
        }
    }


    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private final UUID tempUuid;

        public ConnectThread(BluetoothDevice device, UUID uuidToTry) {
            mmDevice = device;
            BluetoothSocket tmp = null;
            tempUuid = uuidToTry;

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                tmp = device.createRfcommSocketToServiceRecord(tempUuid);        	
            } catch (IOException e) {
                Log.e(TAG, "create() failed", e);
            }
            mmSocket = tmp;
        }

        public synchronized void run() {
        	 Boolean added = mAddressDevices.add(mmDevice.getAddress());
             if (!added) {
             	Message msg = mHandler.obtainMessage(BluetoothChat.MESSAGE_TOAST);
             	Bundle bundle = new Bundle();
             	bundle.putString(BluetoothChat.TOAST, " You're already connecting to this device");
             	msg.setData(bundle);
             	mHandler.sendMessage(msg);
             	setState(STATE_CONNECTED, NO_CLEAR_CHAT_HISTORY);
             	return;
             }
        	
        	
        	// We put the name in connect() when we create the thread
        	Log.i(TAG, "BEGIN mConnectThread " + getName());

        	// Always cancel discovery because it will slow down a connection
        	mAdapter.cancelDiscovery();

        	// Make a connection to the BluetoothSocket
        	try {
        		// This is a blocking call and will only return on a
        		// successful connection or an exception
        		Log.i(TAG, "About to wait to connect to " + mmDevice.getName());
        		mmSocket.connect();		
        	} catch (IOException e) {
        		synchronized (BluetoothChatService.this) {
        			// this attempt has been failed
        			attemps++;
        			if(added) {
        				mAddressDevices.remove(mmDevice.getAddress());
        			}
        			Log.d(TAG, "connection fail, attemps:" + attemps);
        			// Close the socket
        			try {
        				mmSocket.close();
        			} catch (IOException e2) {
        				Log.e(TAG, "unable to close() socket during connection failure", e2);
        			}
        			//see if the others attempts have failed too
        			if (attemps >= 3) {
        				connectionFailed(); //all attempts have been failed
        				attemps = 0;
        			} else {
        				//re-connect
        	        	connect(mmDevice);
        			}
        			// Start the service over to restart listening mode
        			//BluetoothChatService.this.start(); //to remove
        			Log.i(TAG, "END mConnectThread " + getName() + " FAIL");
        			return;
        		}
        	}
        	Log.i(TAG, "connection done, device:" + mmDevice.getName());
        	// Reset the ConnectThread because we're done
        	synchronized (BluetoothChatService.this) {
        		mConnectThread = null;
        		attemps = 0;
        	}
        	// Start the connected thread
        	connected(mmSocket, mmDevice);
        	Log.i(TAG, "END mConnectThread " + getName());
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "unable to close() socket", e);
            }
        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket, BluetoothDevice device) {
            Log.i(TAG, "create ConnectedThread: " + socket.getRemoteDevice().getName());
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            mmDevice = device;
            
            Message msg = mHandler.obtainMessage(BluetoothChat.MESSAGE_CONNECTED);
            Bundle b = new Bundle();
            b.putString(BluetoothChat.NUM_CONNECTED, " " + (mConnThreads.size() + 1)); //We add the thread in a later step
        	msg.setData(b);
        	mHandler.sendMessage(msg);
        	
            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "temp sockets not created", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            Log.i(TAG, "BEGIN mConnectedThread: "+ mmSocket.getRemoteDevice().getName());
            byte[] buffer = new byte[1024];
            int bytes;

            // Keep listening to the InputStream while connected
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);
                    //but bytes is only 1024 lenght... write to a aux-file and the process?
                    Object[] a = processMessage(buffer, bytes);
                    byte[] payload = (byte[]) a[1];
                    
                    if (a[0].equals(MessageType.DATA)) {
                    	// Send the obtained bytes to the UI Activity
                    	Message msg = mHandler.obtainMessage(BluetoothChat.MESSAGE_READ, payload.length, -1, payload);
                        Bundle bundle = new Bundle();
                        bundle.putString(BluetoothChat.DEVICE_NAME, mmSocket.getRemoteDevice().getName());
                        msg.setData(bundle);

                        msg.sendToTarget();
                        
                    } else if (a[0].equals(MessageType.SIGNALING)) {
                    	Message msg = mHandler.obtainMessage(BluetoothChat.MESSAGE_TOAST);
                    
                    Bundle bundle = new Bundle();
                    bundle.putString(BluetoothChat.TOAST,  "Signaling from:"+ mmSocket.getRemoteDevice().getName() + " |" + new String(payload, "UTF-8") + "|");
                    msg.setData(bundle);
                    mHandler.sendMessage(msg);
                    }
                    
                } catch (IOException e) {
                    Log.e(TAG, "disconnected, reason:" + e.getLocalizedMessage());
                    connectionLost(mmSocket);
                    break;
                }
            }
            Log.i(TAG, "END mConnectedThread " + mmDevice.getName());
        }
        /**
         * 
         * @param buffer
         * @param bytes
         * @return
         * @throws InvalidProtocolBufferException 
         */
        public Object[] processMessage(byte[] buffer, int bytes) throws InvalidProtocolBufferException {
        	Object[] msgParsed = new Object[2];
        	byte[] buffer_trim = Arrays.copyOfRange(buffer, 0, bytes);
        	
        	Msg message = Msg.parseFrom(buffer_trim);
        	msgParsed[0] = message.getType();
        	String a = new String(msgParsed[0].toString());
        	msgParsed[1] = message.getPayload().toByteArray();
        	
        	return msgParsed;
        }

        /**
         * Write to the connected OutStream.
         * @param buffer  The bytes to write
         */
        public void write(byte[] buffer) {
            try {
                mmOutStream.write(buffer);

                // Share the sent message back to the UI Activity
                mHandler.obtainMessage(BluetoothChat.MESSAGE_WRITE, -1, -1, buffer)
                        .sendToTarget();
            } catch (IOException e) {
                Log.e(TAG, "Exception during write", e);
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "close() of connect socket failed", e);
            }
        }
    }
}
